import Foundation
import Moya
import RxMoya
import RxSwift

class MovieDetailsPresenter {
    let movieDetailsView: MovieDetailsView
    let movieRepository: MovieRepository
    var subscription: Disposable?
    
    init(movieDetailsView: MovieDetailsView) {
        self.movieDetailsView = movieDetailsView
        self.movieRepository = MovieRepository()
    }
    
    func getMovieDetails(movieId: Int) -> Void {
        movieDetailsView.displayLoading()
        subscription = movieRepository.getMovieDetails(movieId).subscribe {[weak self] event in
            switch event {
            case let .success(response):
                self?.movieDetailsView.hideLoading()
                self?.movieDetailsView.displayMovieDetailsView(movie: response)
            case let .error(error):
                self?.movieDetailsView.hideLoading()
                print(error)
            }
        }
    }
    
    func disposeSubscriptions() {
        subscription?.dispose()
    }
}
