import Foundation

protocol MovieDetailsView {
    func displayMovieDetailsView(movie: MovieDetails)
    func displayLoading()
    func hideLoading()
}
