import UIKit
import Moya
import RxMoya
import RxSwift

class MovieDetailsViewController: UIViewController, MovieDetailsView {

    @IBOutlet var movieDetailsImg: UIImageView!
    @IBOutlet var movieTitle: UILabel!
    @IBOutlet var movieDescription: UILabel!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    
    var idMovieToDisplay: Int = 0
    
    var presenter: MovieDetailsPresenter!
    
    var subscription: Disposable?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter = MovieDetailsPresenter(movieDetailsView: self)
        presenter.getMovieDetails(movieId: idMovieToDisplay)
    }
    
    func displayMovieDetailsView(movie: MovieDetails) {
        let url = URL(string: movie.backdropUrl)
        movieDetailsImg.kf.setImage(with: url)
        movieTitle.text = movie.title
        movieDescription.text = movie.overview
    }
    
    func displayLoading() {
        activityIndicator.startAnimating()
        activityIndicator.isHidden = false
    }
    
    func hideLoading() {
        activityIndicator.isHidden = true
        activityIndicator.stopAnimating()
    }
}
