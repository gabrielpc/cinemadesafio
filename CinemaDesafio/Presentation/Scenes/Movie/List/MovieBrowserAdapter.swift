import UIKit
import Moya
import RxMoya
import RxSwift

class MovieBrowserAdapter: NSObject, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    var collectionView: UICollectionView!
    let TILES_PER_ROW = CGFloat(3)
    var movies: [MovieSummary] = []
    
    private let movieSelectedSubject: PublishSubject<MovieSummary> = PublishSubject()
    var movieSelectedObservable: Observable<MovieSummary> { return movieSelectedSubject }
    
    init(collectionView: UICollectionView)  {
        super.init()
        self.collectionView = collectionView
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
    }
    
    func setData(movies: [MovieSummary]){
        self.movies = movies
        self.collectionView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.movies.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "movieCell", for: indexPath) as? MovieCell {
            cell.configureCell(movie: movies[indexPath.row] )
            return cell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        movieSelectedSubject.onNext(movies[indexPath.row])
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let screenWidth = collectionView.bounds.width
        let celWidth = screenWidth/(TILES_PER_ROW + 1)
        let celHeight = (3 * celWidth) / 2
        return CGSize(width: celWidth, height: celHeight)
    }
}


