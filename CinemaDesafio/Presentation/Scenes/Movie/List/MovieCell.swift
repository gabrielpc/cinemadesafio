import UIKit
import Kingfisher

class MovieCell: UICollectionViewCell {
    
    @IBOutlet weak var movieImg: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configureCell(movie: MovieSummary) {
        let url = URL(string: movie.posterUrl)
        movieImg.kf.setImage(with: url)
    }
}
