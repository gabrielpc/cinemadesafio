import UIKit
import Moya
import RxMoya
import RxSwift

class MovieBrowserViewController: UIViewController, MoviesView{
    
 
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    
    let TILES_PER_ROW = CGFloat(3)
    var adapter: MovieBrowserAdapter!
    var presenter: MovieBrowserPresenter!
    
    var movies: [MovieSummary] = []
    var movieIdToPassToDetailsScreen = -1
    
    var subscription: Disposable?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        adapter = MovieBrowserAdapter(collectionView: collectionView)
        subscription = adapter.movieSelectedObservable.subscribe(onNext: { movie in
            self.performSegue(withIdentifier: "toMovieDetails", sender: movie.id)
        })
        presenter = MovieBrowserPresenter(moviesView: self)
        presenter.getMovieSummaryList()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        subscription?.dispose()
        presenter.disposeSubscriptions()
    }

    func displayMoviesView(movies: [MovieSummary]) {
        self.movies = movies
        self.adapter.setData(movies: movies)
    }
    
    func displayLoading() {
        activityIndicator.startAnimating()
        activityIndicator.isHidden = false
    }
    
    func hideLoading() {
        activityIndicator.isHidden = true
        activityIndicator.stopAnimating()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let movieDetailsVC = segue.destination as? MovieDetailsViewController {
            if let thisViewController = sender as? MovieBrowserViewController {
                movieDetailsVC.idMovieToDisplay = thisViewController.movieIdToPassToDetailsScreen
            }
        }
    }
}

