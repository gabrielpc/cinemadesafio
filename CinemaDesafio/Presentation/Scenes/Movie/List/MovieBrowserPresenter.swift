import Foundation
import Moya
import RxMoya
import RxSwift

class MovieBrowserPresenter {
    let moviesView: MoviesView
    let movieRepository: MovieRepository
    var subscription: Disposable?
    
    init(moviesView: MoviesView) {
        self.moviesView = moviesView
        movieRepository = MovieRepository()
    }
    
    func getMovieSummaryList() -> Void {
        moviesView.displayLoading()
        subscription = movieRepository.getMovieSummaryList().subscribe {[weak self] event in
            switch event {
            case let .success(response):
                self?.moviesView.hideLoading()
                self?.moviesView.displayMoviesView(movies: response)
            case let .error(error):
                self?.moviesView.hideLoading()
                print(error)
            }
        }
    }
    
    func disposeSubscriptions() {
        subscription?.dispose()
    }
}
