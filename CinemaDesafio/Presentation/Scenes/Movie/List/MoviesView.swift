import Foundation

protocol MoviesView {
    func displayMoviesView(movies: [MovieSummary])
    func displayLoading()
    func hideLoading()
}
