struct MovieSummary {
    let id: Int
    let title: String
    let posterUrl: String
}
