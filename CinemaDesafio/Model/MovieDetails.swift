struct MovieDetails {
    let id: Int
    let title: String
    let overview: String
    let backdropUrl: String
    let voteAverage: Double
    let genres: [String]
}
