import SwiftyUserDefaults
import RxSwift

struct MovieCDS {
    
    func upsert(_ movies: [MovieSummaryCM]) -> Completable {
        return Completable.empty().do(onCompleted: {
            Defaults[.movieSummaryList] = movies
        })
    }
    
    func upsertMovieDetails(_ movie: MovieDetailsCM) -> Completable {
        return Completable.empty().do(onCompleted: {
            Defaults[self.getMovieDetailsKeyName(movie.id)] = movie
        })
    }
    
    func getMovieSummaryList() -> Single<[MovieSummaryCM]?> {
        return Observable.create({ (observer: AnyObserver<[MovieSummaryCM]?>) -> Disposable in
            observer.onNext(Defaults[.movieSummaryList])
            return Disposables.create()
        }).take(1).asSingle()
    }
    
    func getMovieDetail(_ movieId: Int) -> Single<MovieDetailsCM?> {
        return Observable.create({ (observer: AnyObserver<MovieDetailsCM?>) -> Disposable in
            observer.onNext(Defaults[self.getMovieDetailsKeyName(movieId)])
            return Disposables.create()
        }).take(1).asSingle()
    }
    
    func getMovieDetailsKeyName(_ movieId: Int) -> DefaultsKey<MovieDetailsCM?> {
        return DefaultsKey<MovieDetailsCM?>("movieDetail-\(movieId)")
    }
}

extension DefaultsKeys {
    static let movieSummaryList = DefaultsKey<[MovieSummaryCM]?>("MovieSummaryList")
}
