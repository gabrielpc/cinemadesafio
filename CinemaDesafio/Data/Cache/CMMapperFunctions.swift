extension MovieSummaryCM {
    func toDM() -> MovieSummary {
        return MovieSummary(id: id, title: title, posterUrl: posterUrl)
    }
}

extension MovieDetailsCM {
    func toDM() -> MovieDetails {
        return MovieDetails(id: id, title: title, overview: overview, backdropUrl: backdropUrl, voteAverage: voteAverage, genres: genres)
    }
}

extension Array where Element == MovieDetailsCM {
    func toDM() -> [MovieDetails] {
        return self.map({ $0.toDM() })
    }
}
