import SwiftyUserDefaults

struct MovieDetailsCM: Codable, DefaultsSerializable {
    let adult: Bool
    let backdropUrl: String
    let budget: Int
    let genres: [String]
    let homepage: String?
    let id: Int
    let imdbId: String
    let originalLanguage: String
    let originalTitle: String
    let overview: String
    let popularity: Double
    let posterUrl: String
    let releaseDate: String
    let revenue: Int
    let runtime: Int
    let status: String
    let tagline: String
    let title: String
    let video: Bool
    let voteAverage: Double
    let voteCount: Int
}
