import SwiftyUserDefaults

struct MovieSummaryCM: Codable, DefaultsSerializable {
    let id: Int
    let voteAverage: Double
    let title: String
    let posterUrl: String
    let genres: [String]
    let releaseDate: String
}
