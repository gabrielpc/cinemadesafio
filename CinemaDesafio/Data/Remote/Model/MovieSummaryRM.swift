struct MovieSummaryRM: Codable {
    let id: Int
    let voteAverage: Double
    let title: String
    let posterUrl: String
    let genres: [String]
    let releaseDate: String
    
    enum CodingKeys: String, CodingKey {
        case id
        case voteAverage = "vote_average"
        case title
        case posterUrl = "poster_url"
        case genres
        case releaseDate = "release_date"
    }
}
