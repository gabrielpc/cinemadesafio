
extension MovieSummaryRM {
    func toCM() -> MovieSummaryCM {
        return MovieSummaryCM(id: id
        , voteAverage: voteAverage
        , title: title
        , posterUrl: posterUrl
        , genres: genres
        , releaseDate: releaseDate)
    }
}

extension MovieDetailsRM {
    func toCM() -> MovieDetailsCM {
        return MovieDetailsCM(adult: adult
        , backdropUrl: backdropUrl
        , budget: budget
        , genres: genres
        , homepage: homepage
        , id: id
        , imdbId: imdbId
        , originalLanguage: originalLanguage
        , originalTitle: originalTitle
        , overview: overview
        , popularity: popularity
        , posterUrl: posterUrl
        , releaseDate: releaseDate
        , revenue: revenue
        , runtime: runtime
        , status: status
        , tagline: tagline
        , title: title
        , video: video
        , voteAverage: voteAverage
        , voteCount: voteCount)
    }
}
