import Moya
import RxMoya
import RxSwift
import Foundation

class MovieRDS {

    let provider = MoyaProvider<MovieService>()

    func getMovieSummaryList() -> Single<[MovieSummaryRM]> {
        return provider.rx.request(MovieService.getMovies).map([MovieSummaryRM].self)
    }
    func getMovieDetails(_ movieId: Int) -> Single<MovieDetailsRM> {
        return provider.rx.request(MovieService.getMovieDetails(id: movieId)).map(MovieDetailsRM.self)
    }
}
