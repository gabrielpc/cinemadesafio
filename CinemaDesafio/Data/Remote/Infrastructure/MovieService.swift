import Foundation
import Moya

enum MovieService {
    case getMovies
    case getMovieDetails(id: Int)
}

extension MovieService: TargetType {
    var baseURL: URL {
        return URL(string: "https://desafio-mobile.nyc3.digitaloceanspaces.com")!
    }
    
    var path: String {
        switch self {
        case .getMovies:
            return "/movies"
        case .getMovieDetails(let id):
            print ("/movies/\(id)")
            return "/movies/\(id)"
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .getMovies, .getMovieDetails:
            return .get
        }
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var task: Task {
        switch self {
        case .getMovies, .getMovieDetails:
            return .requestPlain
        }
    }
    
    var headers: [String : String]? {
        return ["Content-type": "application/json"]
    }
}
