import RxSwift
import Foundation

class MovieRepository {
    var movieRDS: MovieRDS
    var movieCDS: MovieCDS
    
    init() {
        movieRDS = MovieRDS()
        movieCDS = MovieCDS()
    }
    
    func getMovieSummaryList() -> Single<[MovieSummary]> {
        return movieRDS.getMovieSummaryList().map({ movieSummaryArrayRM in
            movieSummaryArrayRM.map({ movieSummaryRM in
                movieSummaryRM.toCM()
            })
        }).flatMap({ movieSummaryArrayCM in
            self.movieCDS.upsert(movieSummaryArrayCM).andThen(Single.just(movieSummaryArrayCM))
        }).catchError({ error in
            self.movieCDS.getMovieSummaryList().flatMap({ cachedMovieSummaryArray in
                if cachedMovieSummaryArray == nil {
                    return Single.error(error)
                } else {
                    return Single.just(cachedMovieSummaryArray!)
                }
            })
        }).map({ resultMovieSummaryArray in
            resultMovieSummaryArray.map({ resultMovieSummary in
                resultMovieSummary.toDM()
            })
        })
    }
    
    func getMovieDetails(_ movieId: Int) -> Single<MovieDetails> {
        return movieRDS.getMovieDetails(movieId).map({ movieDetailsRM in
            movieDetailsRM.toCM()
        }).flatMap({ movieDetailsCM in
            self.movieCDS.upsertMovieDetails(movieDetailsCM).andThen(Single.just(movieDetailsCM))
        }).catchError( { error in
            self.movieCDS.getMovieDetail(movieId).flatMap({ cachedMovieDetails in
                if cachedMovieDetails == nil {
                    return Single.error(error)
                } else {
                    return Single.just(cachedMovieDetails!)
                }
            })
        }).map({ resultMovieDetails in
            resultMovieDetails.toDM()
        })
    }
}
